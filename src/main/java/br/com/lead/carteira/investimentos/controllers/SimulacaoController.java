package br.com.lead.carteira.investimentos.controllers;

import br.com.lead.carteira.investimentos.models.Simulacao;
import br.com.lead.carteira.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {


    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Simulacao> buscarTodasSimulacoes(){
        Iterable<Simulacao> simulacoes = simulacaoService.buscarTodasSimulacoes();
        return  simulacoes;
    }
}
