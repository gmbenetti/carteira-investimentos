package br.com.lead.carteira.investimentos.repositories;

import br.com.lead.carteira.investimentos.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRespository extends CrudRepository<Investimento, Integer> {

}
