package br.com.lead.carteira.investimentos.repositories;


import br.com.lead.carteira.investimentos.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer>  {


}
