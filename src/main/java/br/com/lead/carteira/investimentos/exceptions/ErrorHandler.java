package br.com.lead.carteira.investimentos.exceptions;

import br.com.lead.carteira.investimentos.exceptions.errors.MensagemDeErro;
import br.com.lead.carteira.investimentos.exceptions.errors.ObjetoDeErro;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.List;

@ControllerAdvice
public class ErrorHandler {

    @Value("${mensagem.padrao.de.validacao}")
    private String mensagemPadrao;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public MensagemDeErro manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception){
        HashMap<String, ObjetoDeErro> erros = new HashMap<>();
        BindingResult resultado = exception.getBindingResult();

        // Lista de erros
        List<FieldError> fieldErrors = resultado.getFieldErrors();

        for(FieldError erro : fieldErrors){
            erros.put(erro.getField(), new ObjetoDeErro(erro.getDefaultMessage(),
                    erro.getRejectedValue().toString()));
        }

        MensagemDeErro mensagemDeErro = new MensagemDeErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
                mensagemPadrao, erros);
        return mensagemDeErro;
    }
}
