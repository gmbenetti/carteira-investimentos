package br.com.lead.carteira.investimentos.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSimulacao;
    private Double valorAplicado;
    private Double valorSimulacao;


    private Integer quantidadeMeses;

    @Column(name = "nome")
    @Size(min = 5, max = 80, message = "O nome deve ter entre 5 à 80 caracteres")
    private String nomeInteressado;

    @Email(message = "O e-mail informado é inválido")
    @NotNull(message = "O e-mail deve ser preenchido")
    private String email;
    private LocalDate data;

    @ManyToOne(cascade = CascadeType.ALL)
    private Investimento investimento;

    public Simulacao() {}

    public Simulacao(Double valorAplicado, Double valorSimulacao, Integer quantidadeMeses, String nomeInteressado, String email, LocalDate data) {
        this.valorAplicado = valorAplicado;
        this.valorSimulacao = valorSimulacao;
        this.quantidadeMeses = quantidadeMeses;
        this.nomeInteressado = nomeInteressado;
        this.email= email;
        this.data = data;
    }

    @JsonIgnore
    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    @JsonIgnore
    public Integer getIdSimulacao() {
        return idSimulacao;
    }

    public void setIdSimulacao(Integer idSimulacao) {
        this.idSimulacao = idSimulacao;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) { this.valorAplicado = valorAplicado; }

    @JsonIgnore
    public Double getValorSimulacao() {
        return valorSimulacao;
    }

    public void setValorSimulacao(Double valorSimulacao) { this.valorSimulacao = valorSimulacao; }

    public Integer getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(Integer quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public Investimento getInvestimento() {
        return investimento;
    }

    public Integer getInvestimentoID(){
        return this.getInvestimento().getIdInvestimento();
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}
