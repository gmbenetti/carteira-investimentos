package br.com.lead.carteira.investimentos.services;

import br.com.lead.carteira.investimentos.models.Investimento;
import br.com.lead.carteira.investimentos.models.Simulacao;
import br.com.lead.carteira.investimentos.models.dtos.SimulacaoDTO;
import br.com.lead.carteira.investimentos.repositories.InvestimentoRespository;
import br.com.lead.carteira.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoRespository investimentoRespository;

    public Iterable<Simulacao> buscarTodasSimulacoes() {
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();

        return simulacoes;
    }

    public SimulacaoDTO salvarSimulacao(Simulacao simulacao, Integer idInvestimento) {
        Optional<Investimento> investimento = investimentoRespository.findById(idInvestimento);

        if (investimento.isPresent()) {
            try {
                Double valorAplicado = fixarCasasDecimais(simulacao.getValorAplicado(), 2);
                Double valorMontante = retornaValorMontante(simulacao, investimento.get().getRendimentoAoMes());

                Simulacao simulacaoObjeto = new Simulacao (valorAplicado, valorMontante, simulacao.getQuantidadeMeses(),
                                simulacao.getNomeInteressado(),simulacao.getEmail(),LocalDate.now());

                simulacaoObjeto.setInvestimento(investimento.get());
                simulacaoObjeto = simulacaoRepository.save(simulacaoObjeto);

                SimulacaoDTO retornoSimulacaoDTO = new SimulacaoDTO(investimento.get().getRendimentoAoMes(), simulacaoObjeto.getValorSimulacao());

                return retornoSimulacaoDTO;

            } catch (Exception exception) {
                throw new RuntimeException("Erro ao tentar salvar a simulação");
            }
        } else {
            throw new RuntimeException("Não existe Investimento para o Id informado");
        }
    }

    private Double retornaValorMontante(Simulacao simulacao, Double rendimentoAoMes) {
        Double valorSimulacao = simulacao.getValorAplicado();
        Integer meses = simulacao.getQuantidadeMeses();

        for (int i = 1; i <= meses; i++) {
            valorSimulacao = valorSimulacao * (1 + (rendimentoAoMes / 100));
        }
        return fixarCasasDecimais(valorSimulacao, 2);
    }

    private Double fixarCasasDecimais(Double valor, int casasDecimais) {
        BigDecimal bd = new BigDecimal(valor).setScale(casasDecimais, RoundingMode.FLOOR);
        return bd.doubleValue();
    }
}
