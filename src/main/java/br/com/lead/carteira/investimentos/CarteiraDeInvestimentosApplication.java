package br.com.lead.carteira.investimentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarteiraDeInvestimentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarteiraDeInvestimentosApplication.class, args);
	}

}
