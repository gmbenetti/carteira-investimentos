package br.com.lead.carteira.investimentos.models;

import javax.persistence.*;

@Entity
public class Investimento {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idInvestimento;

    @Column(unique = true)
    private String nome;
    private Double rendimentoAoMes;

    public Investimento() {}

    public Integer getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(Integer idInvestimento) {
        this.idInvestimento = idInvestimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(Double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}
