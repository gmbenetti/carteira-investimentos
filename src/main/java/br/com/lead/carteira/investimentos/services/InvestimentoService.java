package br.com.lead.carteira.investimentos.services;

import br.com.lead.carteira.investimentos.models.Investimento;
import br.com.lead.carteira.investimentos.repositories.InvestimentoRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRespository investimentoRespository;

    public Investimento salvarInvestimento(Investimento investimento) {
        Investimento investimentoObjeto = investimentoRespository.save(investimento);
        return investimentoObjeto;
    }

    public Iterable<Investimento>  buscarTodosOsInvestimentos() {
        Iterable<Investimento> investimentos = investimentoRespository.findAll();
        return  investimentos;
    }
}
