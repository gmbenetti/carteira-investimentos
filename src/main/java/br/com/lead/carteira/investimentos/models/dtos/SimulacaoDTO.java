package br.com.lead.carteira.investimentos.models.dtos;

public class SimulacaoDTO {

    private Double rendimentoPorMes;
    private Double montante;

    public SimulacaoDTO() {}

    public SimulacaoDTO(Double rendimentoPorMes, Double montante) {
        this.rendimentoPorMes = rendimentoPorMes;
        this.montante = montante;
    }

    public Double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(Double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public Double getMontante() {
        return montante;
    }

    public void setMontante(Double montante) {
        this.montante = montante;
    }
}
