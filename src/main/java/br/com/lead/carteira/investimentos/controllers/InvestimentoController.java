package br.com.lead.carteira.investimentos.controllers;


import br.com.lead.carteira.investimentos.models.Investimento;
import br.com.lead.carteira.investimentos.models.Simulacao;
import br.com.lead.carteira.investimentos.models.dtos.SimulacaoDTO;
import br.com.lead.carteira.investimentos.services.InvestimentoService;
import br.com.lead.carteira.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;


    @PostMapping
    public ResponseEntity<Investimento>  registrarInvestimento(@RequestBody Investimento investimento){
        Investimento investimentoObjeto = investimentoService.salvarInvestimento(investimento);
        return ResponseEntity.status(201).body(investimentoObjeto);
    }

    @GetMapping
    public Iterable<Investimento> buscarTodosInvestimentos(){
        Iterable<Investimento> investimentos = investimentoService.buscarTodosOsInvestimentos();
        return investimentos;
    }

    @PostMapping("/{id}/simulacao")
    public SimulacaoDTO salvarSimulacao(@PathVariable int id, @RequestBody @Valid  Simulacao simulacao){
        try{
            SimulacaoDTO simulacaoDTOObjeto = simulacaoService.salvarSimulacao(simulacao, id);
            return simulacaoDTOObjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
