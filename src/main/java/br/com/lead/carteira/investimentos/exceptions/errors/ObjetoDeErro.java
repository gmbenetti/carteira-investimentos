package br.com.lead.carteira.investimentos.exceptions.errors;

public class ObjetoDeErro {
    private String mensagemDeErro;
    private String valorRejeitado;

    public ObjetoDeErro() {}

    public ObjetoDeErro(String mensagemDeErro, String valorRejeitado) {
        this.mensagemDeErro = mensagemDeErro;
        this.valorRejeitado = valorRejeitado;
    }

    public String getMensagemDeErro() {
        return mensagemDeErro;
    }

    public void setMensagemDeErro(String mensagemDeErro) {
        this.mensagemDeErro = mensagemDeErro;
    }

    public String getValorRejeitado() {
        return valorRejeitado;
    }

    public void setValorRejeitado(String valorRejeitado) {
        this.valorRejeitado = valorRejeitado;
    }
}
